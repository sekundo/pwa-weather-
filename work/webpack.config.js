const webpack = require("webpack");
const path = require("path");

let config = {
  entry: "./src/app.js",
  output: {
    path: path.resolve(__dirname, "./public/scripts"),
    filename: "./bundle.js"
  }
}

module.exports = config;
